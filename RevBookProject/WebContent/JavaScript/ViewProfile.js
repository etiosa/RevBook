/**
 * 
 */
function displayProfileInformation()
{

	var xhr = new XMLHttpRequest();	
	var user_profile="";
	xhr.onreadystatechange =function() {
		if(xhr.readyState == 4&& xhr.status == 200){
			var data = xhr.responseText;
			var jsondata = JSON.parse(data);
			
			user_profile= jsondata.firstName + " " + jsondata.lastName;

			document.getElementById("userinfo").innerHTML=user_profile;
	        // sets the user information in the edit information modal
	        document.getElementById("mFirstName").value = jsondata.firstName;
	        document.getElementById("mLastName").value = jsondata.lastName;
	        var genderRadios = document.getElementsByName("sex");
	        if (jsondata.gender == "Male") {
	        	genderRadios[0].checked = true;
	        }
	        else if (jsondata.gender == "Female") {
	        	genderRadios[1].checked = true;
	        }
	        else {
	        	genderRadios[2].checked = true;
	        }
	        document.getElementById("mEmail").value = jsondata.email;
	        document.getElementById("mPhoneNumber").value = jsondata.phoneNumber;
		}
	}
	xhr.open('GET', 'GetMyInfoServlet', true);
	xhr.send();
	
};


