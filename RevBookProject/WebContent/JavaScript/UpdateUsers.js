/**
 * 
 */

function loadInfoModal() {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var servletOutput = this.responseText;
            var obj = JSON.parse(servletOutput);		
            
            // sets the user information on the profile page
            document.getElementById("firstName").innerHTML = obj.firstName;
            document.getElementById("lastName").innerHTML = obj.lastName;
            document.getElementById("gender").innerHTML = obj.gender;
            document.getElementById("email").innerHTML = obj.email;
            document.getElementById("phoneNumber").innerHTML = obj.phoneNumber;
            
        }
	};
    xhr.open('GET', 'getUserInfo', true);
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("Pragma", "no-cache");
    xhr.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
    xhr.send();
};