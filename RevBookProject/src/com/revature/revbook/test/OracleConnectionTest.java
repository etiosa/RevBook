package com.revature.revbook.test;

import static org.junit.Assert.assertNotNull;

import java.sql.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.revature.revbook.dao.OracleConnection;

public class OracleConnectionTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetOracleConnection() {
	
	Connection connection = OracleConnection.getOracleConnection();
	assertNotNull(connection);
		try {
			Statement stmt = connection.createStatement();
			String sql = "select U_FIRSTNAME, U_LASTNAME from REVBOOK_USERS";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				System.out.println(rs.getString("U_FIRSTNAME") + " " + rs.getString("U_LASTNAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
