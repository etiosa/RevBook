package com.revature.revbook.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.*;

import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.revature.revbook.dao.DAO;
import com.revature.revbook.dao.DAOImpl;
import com.revature.revbook.resources.Message;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

public class DAOImplTest {
	private DAO dao;

	@Before
	public void setUp() throws Exception {
		dao = new DAOImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetUserID() {
		assertTrue(dao.getUserID("slavelle") == 1);
		assertTrue(dao.getUserID("aburch") == 2);
		assertTrue(dao.getUserID("sbarnes") == 3);
		assertTrue(dao.getUserID("zkombet") == 4);
	}

	@Test
	public void testGetUserPosts() throws SQLException {
		ResultSet rs = dao.getUserPosts(0);
		while (rs.next()) {
			System.out.println(rs.getInt("U_ID") + ": " + rs.getString("P_CONTENTS"));
		}
	}

	@Test
	public void testSelectUser() throws SQLException {
		ResultSet rs = dao.selectUser(1);
		int uid = 0;
/*		while(rs.next()) {
			uid = rs.getInt("U_ID");
		}
		assertTrue(uid == 1);
		
		rs = dao.selectUser(2);
		while(rs.next()) {
			uid = rs.getInt("U_ID");
		}
		assertTrue(uid == 2);
		
		rs = dao.selectUser(9);
		assertFalse(rs.next());*/
			
		uid = -1;
		rs = dao.selectUser("Alex", "Burch");
		while(rs.next()) {
			uid = rs.getInt("U_ID");
			System.out.println("email = " + rs.getString("U_EMAIL"));
			System.out.println("phone = " + rs.getString("U_PHONENUMBER"));
		}
		assertTrue(uid == 2);
	}
	
	@Test
	public void testUpdateUser() throws SQLException {
		//User user = new User(1, "slavelle", "1234","Shannon", "Lavelle", "sl@email.com", 2, "2676261490");
		//dao.updateUser(user);
	}
	@Test 
	public void HashTagISNotIn_Database() 
	{
		
		DAOImpl doa = new DAOImpl();
		int test= doa.HashTag_Post("Hello");
		assertEquals(test, 0);
	}
	@Test
	public void HashTag_In_DataBase()
	{
		 DAOImpl doa = new DAOImpl();
		 int hashtag = doa.HashTag_Post("#Hello");
		 System.out.println(hashtag);
		 assertEquals(hashtag, 0);
	 
	}
	@Test 
	public void ReturnPost_ID() {
		
		DAOImpl dao= new DAOImpl();
		int get_value=dao.getPostID("This is awesomesss");
		assertEquals(get_value, 0);
		
	} 
	
	@Test
	public void sendMessage()
	{
		DAOImpl dao = new DAOImpl();
		int senderID = 1;
		int receiverID = 2;
		Message message = new Message(senderID, receiverID,"The message was sent");
		int messageResult = dao.sendMessage(message);
		assertEquals(messageResult, 0);
	}
	
	@Test
	public void deletePost()
	{
		DAOImpl dao = new DAOImpl();
		int postID = 1;
		Post post = new Post(postID);
	}


}
