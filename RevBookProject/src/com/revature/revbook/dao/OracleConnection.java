package com.revature.revbook.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class OracleConnection {

	private static final Logger LOGGER = LogManager.getLogger(OracleConnection.class);
	private static Connection conn;

	private OracleConnection() {
		try {
			LOGGER.info("creating OracleConnection");
			
			// better way to get the connection - weblogic server must be running
			/*Hashtable<String, String> h = new Hashtable<String, String>(7);
			h.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
			h.put(Context.PROVIDER_URL, "t3://localhost:7001");//add ur url
			h.put(Context.SECURITY_PRINCIPAL, "weblogic");//add username
			h.put(Context.SECURITY_CREDENTIALS, "welcome1");//add password */

			//Context initContext = new InitialContext(h);
			//DataSource ds = (DataSource) initContext.lookup("oracleREVBOOKds");
			//conn = ds.getConnection();
			
/*			//2 - Load Driver
			Class.forName("oracle.jdbc.driver.OracleDriver"); */
			
			//3 - Get connection object
			String url = "jdbc:oracle:thin:@localhost:1521:xe";
			String username = "RevBook";
			String password = "p4ssw0rd";
			conn = DriverManager.getConnection(url, username, password);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getOracleConnection() {
		LOGGER.info("getting OracleConnection");
		if (conn == null) {
			new OracleConnection();
		}
		if (conn == null) {
			LOGGER.warn("failed to create connection to database");
		}
		return conn;
	}
}