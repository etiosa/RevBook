package com.revature.revbook.dao;

import java.sql.ResultSet;

import com.revature.revbook.resources.Message;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

public interface DAO {
	
	public int authenticate(String username, String password);
	public int getUserID(String username);
	public ResultSet getUserPosts(int userID);
	public ResultSet receiveMessages(int userID);
	public ResultSet selectUser(int userID);
	public ResultSet selectUser(String fname, String lname);
	public void updateUser(User user);
	
	public boolean addPost(Post post);
	public void editPost(Post post);
	public void deletePost(int postID);
	
	public void Create_New_Users(String Username, String Password, String firstName, String lastName, String email, String PhoneNumber,String Gender);
	//public void addUser(User user);
	void FindHashTag(String post, int post_id);
	
	public int HashTag_Post(String hash_Tag);
	
	public int getPostID(String post_contents);
	public int sendMessage(Message message);
	public void updateLikes(int post_id, int likes);
	
}