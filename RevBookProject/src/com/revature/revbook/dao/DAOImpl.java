package com.revature.revbook.dao;



import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.revature.revbook.ServerMessage.ServiceMessage;
import com.revature.revbook.resources.Message;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

import oracle.jdbc.OracleTypes;

public class DAOImpl implements DAO {
	private static final Logger LOGGER = LogManager.getLogger(DAOImpl.class);
	private static final String TokenMatches= "\\.|\\s|\\?|\\*|\\!|\\@|\\$|\\%|\\^|\\&|\\(|\\)|\\-|\\+|_\\|\\=|\\,|\\<|\\>";
	private static final char hashtag ='#';

	private Connection conn;
	
	public DAOImpl() {
		LOGGER.info("in DAOImpl constructor");
		conn = OracleConnection.getOracleConnection();
	}
	
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			LOGGER.warn("failed to close Oracle Connection");
			e.printStackTrace();
		}
	}
	
	@Override
	public int authenticate(String username, String password) {
		LOGGER.info("in authenticate");
		// create Callable Statement
		try {
			LOGGER.info("calling AUTH(?,?,?)");
			CallableStatement cs = conn.prepareCall("call AUTH(?,?,?)");
			// set parameters
			cs.setString(1, username);			
			cs.setString(2, password);			
			cs.registerOutParameter(3, OracleTypes.NUMBER);
			// execute statement
			cs.executeQuery();
			int userID = cs.getInt(3);
			LOGGER.debug("userID: " + userID);
            return userID;
		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return 0;	
	}

	@Override
	public int getUserID(String username) {
		LOGGER.info("in getUserID");
		// create Callable Statement
		try {
			LOGGER.info("calling GETUSERID(?,?)");
			CallableStatement cs = conn.prepareCall("call GETUSERID(?,?)");
			// set parameters
			cs.setString(1, username);
			cs.registerOutParameter(2, OracleTypes.NUMBER);
			// execute statement
			cs.executeQuery();
			int userID = cs.getInt(2);
			LOGGER.debug("userID: " + userID);
			return userID;

		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public ResultSet getUserPosts(int userID) {
		LOGGER.info("in getUserPosts");
		// create Callable Statement
		try {
			LOGGER.info("calling GETUSERPOSTS(?,?)");
			CallableStatement cs = conn.prepareCall("call GETUSERPOSTS(?,?)");
			// set parameters
			cs.setInt(1, userID);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			// execute statement
			cs.executeQuery();
			ResultSet rs = (ResultSet) cs.getObject(2);
			
			return rs;

		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return null;
	}
	public int sendMessage(Message message)
	{
		LOGGER.info("in sendMessage");
		int messageSent = 0;
		//Create Callable Statement
		try
		{
			LOGGER.info("calling SENDMESSAGE(?,?,?)");
			CallableStatement cs = conn.prepareCall("call SENDMESSAGE(?,?,?)");
			
			//Set parameters
			cs.setInt(1, message.getSenderId());
			cs.setInt(2, message.getReceiverId());
			cs.setString(3, message.getContents());
			
			//Execute Statement
			cs.executeUpdate();
			
			LOGGER.debug(ServiceMessage.Successful + "Message added to the Database");
		
			return messageSent;
		}
		catch(SQLException e)
		{
			LOGGER.fatal(ServiceMessage.Failed + " to add message to Database");
			e.printStackTrace();
			LOGGER.fatal("SQLException");
			return messageSent = 1;
		}
		
	}

	@Override
	public ResultSet receiveMessages(int userID) {
		LOGGER.info("in receiveMessages");
		// create Callable Statement
		try {
			LOGGER.info("calling RECEIVEMESSAGES(?,?)");
			CallableStatement cs = conn.prepareCall("call RECEIVEMESSAGES(?,?)");
			// set parameters
			cs.setInt(1, userID);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			// execute statement
			cs.executeQuery();
			ResultSet rs = (ResultSet) cs.getObject(2);
			
			return rs;

		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResultSet selectUser(int userID) {
		LOGGER.info("in selectUser");
		// create Callable Statement
		try {
			LOGGER.info("calling SELECTUSER(?,?)");
			CallableStatement cs = conn.prepareCall("call SELECTUSER(?,?)");
			// set parameters
			cs.setInt(1, userID);
			cs.registerOutParameter(2, OracleTypes.CURSOR);
			// execute statement
			cs.executeQuery();
			ResultSet rs = (ResultSet) cs.getObject(2);
			return rs;

		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return null;
	}
	
	public ResultSet selectUser(String fname, String lname) {
		LOGGER.info("in selectUser");
		// create Callable Statement
		try {
			LOGGER.info("calling SELECTUSERFROMNAME(?,?,?)");
			CallableStatement cs = conn.prepareCall("call SELECTUSERFROMNAME(?,?,?)");
			// set parameters
			cs.setString(1, fname);
			cs.setString(2, lname);
			cs.registerOutParameter(3, OracleTypes.CURSOR);
			// execute statement
			cs.executeQuery();
			ResultSet rs = (ResultSet) cs.getObject(3);
			if (rs == null) {
				LOGGER.warn("null result set returned");
			}
			return rs;

		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateUser(User user) {
		LOGGER.info("in updateUser");
		// create Callable Statement
		try {
			LOGGER.info("calling UPDATEUSER(?,?,?,?,?,?,?)");
			CallableStatement cs = conn.prepareCall("call UPDATEUSER(?,?,?,?,?,?,?)");
			// set parameters
			cs.setInt(1, user.getId());
			LOGGER.debug("userID: " + user.getId());
			cs.setString(2, user.getFirstName());
			cs.setString(3, user.getLastName());
			cs.setString(4, user.getEmail());
			cs.setString(5, user.getPhoneNumber());
			cs.setString(6, user.getGender());
			cs.setString(7, user.getPassword());
			// execute statement
			cs.executeQuery();
			
		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
		}
	}

	@Override
	public boolean addPost(Post post) {
		LOGGER.info("in addPost");
		try {
			LOGGER.info("calling ADDPOST(?,?,?)");
			CallableStatement cs = conn.prepareCall("call ADDPOST(?,?,?)");
			// set parameters
			cs.setInt(1, post.getAuthorId());
			cs.setString(2, post.getContents());
			cs.setInt(3, post.getLikes());
			// execute statement
			cs.executeUpdate();
			return true;
		} catch (SQLException e) {
			LOGGER.error("SQLException");
			e.printStackTrace();
			return false;
		}
		
	}
/**
 * @author etiosa obasuyi
 */
	@Override
	public void Create_New_Users(String Username, String Password, String firstName, String lastName, String email,
			String PhoneNumber, String Gender) {

		LOGGER.debug(ServiceMessage.Attempting + " to add new user");
		LOGGER.debug("Calling  ADDUSER(?,?,?,?,?,?,?");
		try {
			CallableStatement new_user = conn.prepareCall("call ADDUSER(?,?,?,?,?,?,? )");
			new_user.setString(1, Username);
			new_user.setString(2, Password);
			new_user.setString(3, firstName);
			new_user.setString(4, lastName);
			new_user.setString(5, email);
			new_user.setString(6, PhoneNumber);
			new_user.setString(7, Gender);
			new_user.executeUpdate();
			LOGGER.debug(ServiceMessage.Successful + " added a new user to the database");
		} catch (SQLException e) {
			LOGGER.fatal(ServiceMessage.Failed + " to add new user to database");
			LOGGER.fatal("SQL Exception");
			return;

		}
	}
	
	
	
	/**@author etiosa obasuyi
	 * 
	 */
	private ArrayList<String> Split_Contents(String contents){
		ArrayList<String>post_tokens= new ArrayList<String>();
		//1. split the contents 
		String []split_post = contents.split(TokenMatches);
		for (int i=0;i<split_post.length;i++) {
			 if(split_post[i].equals("")){
				 continue;
			 }
			  post_tokens.add(split_post[i]);
		}
		return post_tokens;
	}

/** @author etiosa obasuyi
 * 
 */
@Override
public void FindHashTag(String post, int post_id) {
     
	LOGGER.debug(" calling addHashTag(String post, int pos_id)");
	
	//retrive the arraylist 
	ArrayList<String>Tokens = Split_Contents(post);
	//2 iterating through search if word start with #
	for (String search_hash : Tokens) {
		if(search_hash.charAt(0)==hashtag) {
			// search if the HashTag is already in the database
			if(SearchHashTag(search_hash)) {
			//add the word into the database
				addHashTag(search_hash,post_id);
			}
			else {
				LOGGER.debug("HashTag is already in the database");
			}
			}
		
	}
	
	return;
}


private boolean addHashTag(String hashTag, int post_id) {
	LOGGER.debug("Calling ADDHASHTAG(?,?)");

	try {
		CallableStatement addhastag = conn.prepareCall("call ADDHASHTAG(?,?)");
		addhastag.setString(1, hashTag);
		addhastag.setInt(2, post_id);
		addhastag.executeUpdate();
		return true;
		
	} catch (SQLException e) {
		LOGGER.debug(ServiceMessage.Failed + " to add hashtag. SQL Error");
		return false;
	}
	
}

/**@author etiosa obasuyi
 * Check if the HashTag is already in the Database
 * 
 */
private boolean SearchHashTag(String search_hashtag){
	
	try {
		CallableStatement check_hashaTag = conn.prepareCall("call RETURNHASHTAG(?,?)");
		check_hashaTag.setString(1, search_hashtag);
		check_hashaTag.registerOutParameter(2, OracleTypes.VARCHAR);
		check_hashaTag.executeQuery();
		String result_hashtag= check_hashaTag.getString(2);
		if(result_hashtag==null){
			return true;
		}
		else {
			return false;
		}
		
	}catch(SQLException e){
		LOGGER.debug(ServiceMessage.Failed + " SQL command error/or searching Tag was not found");
		return false;
		
	}
}

@Override
public void editPost(Post post) {
	LOGGER.info("in editPost");
	try {
		LOGGER.info("calling EDITPOST(?,?)");
		CallableStatement cs = conn.prepareCall("call EDITPOST(?,?)");
		// set parameters
		cs.setInt(1, post.getId());
		cs.setString(2, post.getContents());
		// execute statement
		cs.executeQuery();
		
	} catch (SQLException e) {
		LOGGER.error("SQLException");
		e.printStackTrace();
	}
	
}
/**@author etiosa obasuyi
 * @return 
 * 
 */
@Override
public void deletePost(int postID) {

	
	
	LOGGER.info("in deletePost");
	try {
		LOGGER.info("calling DELETEPOST(?)");
		CallableStatement cs = conn.prepareCall("call DELETEPOST(?)");
		// set parameters
		cs.setInt(1, postID);
		// execute statement
		cs.executeQuery();
		
		
	} catch (SQLException e) {
		LOGGER.error("SQLException");
		e.printStackTrace();
		
	}

}
/**@author etiosa obasuyi
 * Searching hashtag
 */
@Override
public int HashTag_Post(String hash_Tag) {
    LOGGER.debug(ServiceMessage.Attempting + "  to search for a HashTag");
       int Post_id =0;
    
    try {
		CallableStatement search_hash = conn.prepareCall("call SEARCH_HASHTAG_POST(?,?)");
		search_hash.setString(1, hash_Tag );
		search_hash.registerOutParameter(2, OracleTypes.NUMBER);
		search_hash.executeQuery();
		Post_id = search_hash.getInt(2);
		
		 LOGGER.debug(ServiceMessage.Successful + " find the hashtag");
	
		 return Post_id;
	} catch (SQLException e) {
		LOGGER.fatal(ServiceMessage.Failed + " SQL Hash_Tag error");
		 return Post_id;
		}
	
	}

/**@author etiosa obasuyi
 * Returning the Post ID
 */
@Override
public int getPostID(String post_contents) {
	LOGGER.debug(ServiceMessage.Attempting + " to get the post id");
	int search_post_id =0;
	try {
		CallableStatement return_post_id = conn.prepareCall("call GETPOSTID(?,?)");
		return_post_id.setString(1, post_contents);
		return_post_id.registerOutParameter(2, OracleTypes.NUMBER);
		return_post_id.executeQuery();
		search_post_id= return_post_id.getInt(2);
		LOGGER.debug(ServiceMessage.Successful + " find the post id");
		return search_post_id;
		
	}catch(SQLException e){
		LOGGER.fatal(ServiceMessage.Failed + "  to find post id or sql error");
		return search_post_id;
		
	}
	
	
}
/** @author etiosa obasuyi
 * 
 */
@Override
public void updateLikes(int post_id, int likes) {
	
	LOGGER.debug(ServiceMessage.Attempting+ " to update likes of a post");
	try{
		
		CallableStatement update_likes = conn.prepareCall("call LIKES(?,?)");
		update_likes.setInt(1, post_id);
		update_likes.setInt(2, likes);
		update_likes.executeUpdate();
		LOGGER.debug(ServiceMessage.Successful + " updated a like for a post");
		
	}catch(SQLException e) {
		LOGGER.fatal(ServiceMessage.Failed +" to update likes of a post");
		
		
	}
	
}

}