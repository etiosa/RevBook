package com.revature.revbook.client;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.revature.revbook.bus.UserServiceImpl;
import com.revature.revbook.resources.User;

/**
 * Servlet implementation class CreateNew_Users
 * 
 */
/**@author etiosa obasuyi
 * 
 */

public class CreateNew_Users extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateNew_Users() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//String user_password = "";
		System.out.println("CREATE NEW USER SERVLET");

		//String user_password = "";
		String UserName = request.getParameter("username");
		String Password = request.getParameter("password");
		//String repassword = request.getParameter("Repassword");
		String firstname = request.getParameter("firstName");
		String lastname = request.getParameter("lastName");
		String Email = request.getParameter("email");
		String Gender = request.getParameter("sex");
		String Phone = request.getParameter("phonenumber");
		if (Gender.equals("Male")) {
			Gender = "Male";
		} else if (Gender.equals("Female")) {
			Gender = "Female";
		}
		else 
		{
			Gender = "Other";
		}
		
		User new_user = new User(UserName, Password, firstname, lastname, Email, Gender, Phone);

		UserServiceImpl service = new UserServiceImpl();

		service.Add_User(new_user);
		
		// send the response
		System.out.println(Gender);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}
}
