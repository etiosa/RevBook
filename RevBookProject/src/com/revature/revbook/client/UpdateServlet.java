package com.revature.revbook.client;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.revature.revbook.ServerMessage.ServiceMessage;
import com.revature.revbook.bus.UserService;
import com.revature.revbook.bus.UserServiceImpl;
import com.revature.revbook.dao.DAOImpl;
import com.revature.revbook.resources.User;

/**
 * Servlet implementation class UpdateServlet
 */
/**
 * @author etiosa obasuyi
 */

public class UpdateServlet extends HttpServlet {

	private static final Logger LOGGER = LogManager.getLogger(UpdateServlet.class);
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// get session object
		HttpSession session = request.getSession();

		LOGGER.debug(ServiceMessage.Attempting + " update user information");

		// get current user information from the session object
		User user = (User) session.getAttribute("Login_User");

		// update the user information with data from the form
		user.setFirstName(request.getParameter("firstName"));
		user.setLastName(request.getParameter("lastName"));
		user.setEmail(request.getParameter("email"));
		user.setGender(request.getParameter("sex"));
		user.setPhoneNumber(request.getParameter("phoneNumber"));

		// use a service object to update the database
		UserService service = new UserServiceImpl();
		service.updateUserInformation(user);
		
		// update the User object in the session
		session.setAttribute("Login_User", user);
		
	    response.sendRedirect("ProfilePage.html");
		
/*		 sending the updated information back to the front end 
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		Gson gson = new Gson();
		// String udpated_user = gson.toJson(user);

		PrintWriter out = response.getWriter();
		// out.write(udpated_user);
*/
	}
}
