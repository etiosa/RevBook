package com.revature.revbook.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.revature.revbook.bus.UserService;
import com.revature.revbook.bus.UserServiceImpl;
import com.revature.revbook.resources.User;

/**
 * Servlet implementation class DisplayProfile
 */


public class DisplayProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(DisplayProfile.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("in displayProfile servlet - loading another user's information");
		HttpSession session = request.getSession();
		
		String firstname = request.getParameter("searchedfirstname");
		String lastname = request.getParameter("searchedlastname");
		LOGGER.debug("searching for user: " + firstname + " " + lastname);
		
		UserService service = new UserServiceImpl();
		
		User searchedUser = service.getUserInformation(firstname, lastname);
		
		session.setAttribute("Profile_User", searchedUser);
		
		response.sendRedirect("ProfilePage.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("in displayProfile servlet - loading another user's information");
		HttpSession session = request.getSession();
		
		String firstname = request.getParameter("searchedfirstName");
		String lastname = request.getParameter("searchedlastName");
		LOGGER.debug("searching for user: " + firstname + " " + lastname);
		
		UserService service = new UserServiceImpl();
		
		User searchedUser = service.getUserInformation(firstname, lastname);
		
		session.setAttribute("Profile_User", searchedUser);
		
		response.sendRedirect("ProfilePage.html");
	}

}
