package com.revature.revbook.client;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.revature.revbook.resources.User;
import com.revature.revbook.bus.UserService;
import com.revature.revbook.bus.UserServiceImpl;
import com.revature.revbook.dao.DAOImpl;
import com.revature.revbook.resources.Post;

/**
 * Servlet implementation class ViewUserPostsServlet
 */

public class ViewUserPostsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(ViewUserPostsServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewUserPostsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.info("in view user posts servlet");
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("Profile_User");
		UserService service = new UserServiceImpl();
		LOGGER.debug("User is: " + session.getAttribute("Login_User"));
		LOGGER.debug("Displaying Profile: " + session.getAttribute("Profile_User"));
		
		ArrayList<Post> myPosts = service.getUserPosts(user.getId());
		Gson gson = new Gson();
		
		String json = gson.toJson(myPosts);
		response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String like = request.getParameter("likes");
	String post_id = request.getParameter("POST_ID");
	
	
	System.out.println("likes value is "+like);
	System.out.println("post id for this like is "+ post_id);
	System.out.println("POST IN JAVA SIDE");
	
	int likes=Integer.parseInt(like);
	int in_post_id= Integer.parseInt(post_id);
	DAOImpl doa = new DAOImpl ();
	doa.updateLikes(in_post_id, likes);
	}

}
