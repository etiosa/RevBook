package com.revature.revbook.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.revature.revbook.bus.UserService;
import com.revature.revbook.bus.UserServiceImpl;
import com.revature.revbook.dao.DAOImpl;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

/**
 * Servlet implementation class CreatePostServlet
 */
/**
 * @author etiosa obasuyi
 */


public class CreatePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = LogManager.getLogger(CreatePostServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("INSIDE CREATEPOSTSERVLET");
		LOGGER.info("posting a status");
		DAOImpl doa= new DAOImpl();

		
		// get the contents of the post from the form
		String postContents = request.getParameter("statuspost");
		
			// create a Service object
			UserService service = new UserServiceImpl();
			
			// get the session object and user information
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("Login_User");
			System.out.println("user ID IS "+user.getId());
			// call the addPost method in the service object
			Post post = new Post(user.getId(),postContents);
			service.addPost(post);
			
			int post_id = doa.getPostID(post.getContents()); 
			doa.getPostID(post.getContents());
			
			doa.FindHashTag(post.getContents(), post_id);
		
		    response.sendRedirect("ProfilePage.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
