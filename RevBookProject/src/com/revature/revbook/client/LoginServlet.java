	package com.revature.revbook.client;
	
	import java.io.IOException;
	
	import javax.servlet.RequestDispatcher;
	import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;
	import javax.servlet.http.HttpSession;
	
	import org.apache.log4j.LogManager;
	import org.apache.log4j.Logger;
	
	import com.revature.revbook.ServerMessage.ServiceMessage;
	import com.revature.revbook.bus.UserServiceImpl;
	import com.revature.revbook.dao.DAOImpl;
	import com.revature.revbook.resources.User;
	
	/**
	 * Servlet implementation class LoginServlet
	 */
	/**
	 * @author etiosa obasuyi
	 */

	public class LoginServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;
		private static final Logger LOGGER = LogManager.getLogger(LoginServlet.class);
	
		/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.info("Inside LoginServlet");
		LOGGER.debug(ServiceMessage.Attempting + " to login");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String page = "";
		UserServiceImpl service = new UserServiceImpl();

		/* info */
		System.out.println(username + " " + password);

		// return the user object
		User user = service.login(username, password);
		int user_id = user.getId();
		System.out.println(user_id);
		if (user_id > 0) {
			LOGGER.debug(ServiceMessage.Successful + " login");
			// will take the users to the homepage
			HttpSession session = request.getSession();
			// save the information of the user who is logged in
			session.setAttribute("Login_User", user);
			// save the information of the user whose page we are displaying
			session.setAttribute("Profile_User", user);
			LOGGER.debug("User is: " + session.getAttribute("Login_User"));
			LOGGER.debug("Displaying Profile: " + session.getAttribute("Profile_User"));

			page = "ProfilePage.html";

		} else {
			// page redirect the user back to the Login Page or error message
			page = "../Pages/Fail.html";
			LOGGER.debug("Login was on " + ServiceMessage.Unscessful);
		}

		response.sendRedirect(page);
		
/*		RequestDispatcher fr = request.getRequestDispatcher(page);

		// forward or redirect user
		fr.forward(request, response);
*/
	}

}

