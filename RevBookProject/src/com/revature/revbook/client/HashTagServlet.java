package com.revature.revbook.client;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.revature.revbook.dao.DAOImpl;

/**
 * Servlet implementation class HashTagServlet
 */
/**
 * @author etiosa obasuyi
 */

public class HashTagServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HashTagServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("INSIDE HASHTAG");
		String search_hash_tag = request.getParameter("search_Hash_Tag_Post");
		System.out.println(search_hash_tag);
		
		RequestDispatcher fr = request.getRequestDispatcher("../Pages/SearchResult.html");
		
		response.sendRedirect("../Pages/SearchResult.html");
	
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);

		
		
	}

}
