package com.revature.revbook.bus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.revature.revbook.ServerMessage.ServiceMessage;
import com.revature.revbook.dao.DAO;
import com.revature.revbook.dao.DAOImpl;
import com.revature.revbook.resources.Message;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
	private DAO dao;
	
	public UserServiceImpl() {
		// get DAO instance
		dao = new DAOImpl();
	}
	
	public User login(String username, String password) {
		LOGGER.info("in login");
		
		// get userID from the database
		int userID = dao.authenticate(username, password);
		User user = new User();
		user.setId(0);
		
		// check that a user was found
		if (userID == 0) {
			return user;
		}
		
		// get the User information from the database
		ResultSet rs = dao.selectUser(userID);
		try {
			while (rs.next()) {
			  
				user.setId(rs.getInt("U_ID"));
				user.setUsername(rs.getString("U_USERNAME"));
				user.setFirstName(rs.getString("U_FIRSTNAME"));
				user.setLastName(rs.getString("U_LASTNAME"));
				user.setEmail(rs.getString("U_EMAIL"));
				user.setPhoneNumber(rs.getString("U_PHONENUMBER"));
				user.setGender(rs.getString("U_GENDER"));
				user.setPassword(rs.getString("U_PASSWORD"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public void Add_User(User new_users) {
		LOGGER.debug("Add(Users) called");
		 dao.Create_New_Users(new_users.getUsername(), new_users.getPassword(), new_users.getFirstName(), new_users.getLastName(), new_users.getEmail(),  new_users.getPhoneNumber(), new_users.getGender());
	}
	
	@Override	
	public User getUserInformation(int userID) {
		LOGGER.info("in getUserInformation");
		
		ResultSet rs = dao.selectUser(userID);
		try {		
			int id = 0;
			while (rs.next()) {
				id = rs.getInt("U_ID");
				String username = rs.getString("U_USERNAME");
				String firstname = rs.getString("U_FIRSTNAME");
				String lastname = rs.getString("U_LASTNAME");
				String email = rs.getString("U_EMAIL");
				String phoneNumber = rs.getString("U_PHONENUMBER");
				String gender = rs.getString("U_GENDER");
				LOGGER.debug("id: " + id + "; username: " + username + "; firstname: " + firstname + "; lastname: " + lastname);
				return new User(username, null, firstname, lastname, email, gender, phoneNumber);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public User getUserInformation(String fname, String lname) {
		LOGGER.info("in getUserInformation");
		
		User user = new User();
		user.setId(0);
		ResultSet rs = dao.selectUser(fname, lname);
		try {		
			while (rs.next()) {
				user.setId(rs.getInt("U_ID"));
				user.setUsername(rs.getString("U_USERNAME"));
				user.setFirstName(rs.getString("U_FIRSTNAME"));
				user.setLastName(rs.getString("U_LASTNAME"));
				user.setEmail(rs.getString("U_EMAIL"));
				user.setPhoneNumber(rs.getString("U_PHONENUMBER"));
				user.setGender(rs.getString("U_GENDER"));
				LOGGER.debug("id: " + user.getId() + "; username: " + user.getUsername() + "; firstname: " + user.getFirstName() + "; lastname: " + user.getLastName());
			}
		} catch (SQLException e) {
			LOGGER.warn("failed to retrieve user from database");
			e.printStackTrace();
		}
		return user;
	}
	
	@Override
	public void updateUserInformation(User u) {
		LOGGER.info("in updateUserInformation");
		
		dao.updateUser(u);
	}

	@Override
	public ArrayList<Post> getUserPosts(int userId) {
		LOGGER.info("in getUserPosts");
		
		ArrayList<Post> posts = new ArrayList<Post>();
		try {
			ResultSet rs = dao.getUserPosts(userId);
			while (rs.next()) {
				Post p = new Post(rs.getInt("P_ID"), rs.getInt("U_ID"), rs.getTimestamp("P_TIMEPOSTED"), rs.getString("P_CONTENTS"), rs.getInt("LIKES"));
				posts.add(p);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOGGER.debug("retrieved posts for userID: " + userId);
		LOGGER.debug("number of posts returned: " + posts.size());
		return posts;
	}

	@Override
	public void addPost(Post post) {
		LOGGER.info("in addPost");
		
		dao.addPost(post);
	}
	
	@Override
	public void editPost(Post post) {
		LOGGER.info("in editPost");
		dao.editPost(post);
	}

	@Override
	public void deletePost(int postID) {
		LOGGER.info("in deletePost");
		dao.deletePost(postID);
	}

	@Override
	public ArrayList<Post> Return_HashTag_Post(String search_Hash_Tag) {
		//1. Get the post id  that matches the hashtag table ... 
		//2. Use the post id to get  all u_id and post contents that include that hashtag
		
		DAOImpl dao = new DAOImpl();
		int post_id=dao.HashTag_Post(search_Hash_Tag);

		return null;
	}
	
	public void sendMessage(Message message)
	{
		LOGGER.info("in sendMessage");
		dao.sendMessage(message);
	}
}

