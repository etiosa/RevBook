package com.revature.revbook.bus;

import java.util.ArrayList;

import com.revature.revbook.resources.Message;
import com.revature.revbook.resources.Post;
import com.revature.revbook.resources.User;

public interface UserService {
	public User getUserInformation(int userID);
	public User getUserInformation(String fname, String lname);
	public void updateUserInformation(User u);
	public ArrayList<Post> getUserPosts(int userID);
//	public ArrayList<User> getUsersFollowing(int userId);
	public void addPost(Post p);
	public void Add_User(User new_users);
//	public void addFollowing(int myUserID, int followingID);
	void editPost(Post post);
	void deletePost(int postID);
	void sendMessage(Message message);
	
	public ArrayList<Post>Return_HashTag_Post(String search_Hash_Tag);
}
