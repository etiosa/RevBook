package com.revature.revbook.resources;

import java.sql.Timestamp;

public class Message {
	
	private int id;
	private int senderId;
	private int receiverId;
	private Timestamp timeSent;
	private String contents;
	
	public Message(int id, int senderId, int receiverId, Timestamp timeSent, String contents) {
		super();
		this.id = id;
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.timeSent = timeSent;
		this.contents = contents;
	}
	


	public Message(int senderId, int receiverId, String contents) {
		super();
		this.senderId = senderId;
		this.receiverId = receiverId;
		this.contents = contents;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public int getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public Timestamp getTimeSent() {
		return timeSent;
	}
	public void setTimeSent(Timestamp timeSent) {
		this.timeSent = timeSent;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}

	public String toString() {
		return "Message [id=" + id + ", senderId=" + senderId + ", receiverId=" + receiverId + ", timeSent=" + timeSent
				+ ", contents=" + contents + "]";
	}
}
