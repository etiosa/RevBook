package com.revature.revbook.resources;

import java.sql.Timestamp;

public class Post {
	
	private int id;
	private int authorId;
	private Timestamp timePosted;
	private String contents;
	private int likes;
	
	public Post(int id, int authorId, Timestamp timePosted, String contents, int likes) {
		super();
		this.id = id;
		this.authorId = authorId;
		this.timePosted = timePosted;
		this.contents = contents;
		this.likes=likes;
	}
	
	public Post(int authorId, String contents) {
		super();
		this.authorId = authorId;
		this.contents = contents;
	}

	
	public Post(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAuthorId() {
		return authorId;
	}
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	public Timestamp getTimePosted() {
		return timePosted;
	}
	public void setTimePosted(Timestamp timePosted) {
		this.timePosted = timePosted;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}

	public String toString() {
		return "Post [id=" + id + ", authorId=" + authorId + ", timePosted=" + timePosted + ", contents=" + contents
				+ "]";
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}
}
